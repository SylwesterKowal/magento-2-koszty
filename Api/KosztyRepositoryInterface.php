<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface KosztyRepositoryInterface
{

    /**
     * Save Koszty
     * @param \Kowal\Koszty\Api\Data\KosztyInterface $koszty
     * @return \Kowal\Koszty\Api\Data\KosztyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\Koszty\Api\Data\KosztyInterface $koszty
    );

    /**
     * Retrieve Koszty
     * @param string $kosztyId
     * @return \Kowal\Koszty\Api\Data\KosztyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($kosztyId);

    /**
     * Retrieve Koszty matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\Koszty\Api\Data\KosztySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Koszty
     * @param \Kowal\Koszty\Api\Data\KosztyInterface $koszty
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\Koszty\Api\Data\KosztyInterface $koszty
    );

    /**
     * Delete Koszty by ID
     * @param string $kosztyId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($kosztyId);
}

