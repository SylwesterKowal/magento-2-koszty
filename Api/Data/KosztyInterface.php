<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Api\Data;

interface KosztyInterface
{

    const KOSZT_NOWY = 'koszt_nowy';
    const KOSZTY_ID = 'koszty_id';
    const STORE_ID = 'store_id';
    const KOSZT_STARY = 'koszt_stary';
    const NAZWA_BAZOWA = 'nazwa_bazowa';

    /**
     * Get koszty_id
     * @return string|null
     */
    public function getKosztyId();

    /**
     * Set koszty_id
     * @param string $kosztyId
     * @return \Kowal\Koszty\Koszty\Api\Data\KosztyInterface
     */
    public function setKosztyId($kosztyId);

    /**
     * Get store_id
     * @return string|null
     */
    public function getStoreId();

    /**
     * Set store_id
     * @param string $storeId
     * @return \Kowal\Koszty\Koszty\Api\Data\KosztyInterface
     */
    public function setStoreId($storeId);

    /**
     * Get nazwa_bazowa
     * @return string|null
     */
    public function getNazwaBazowa();

    /**
     * Set nazwa_bazowa
     * @param string $nazwaBazowa
     * @return \Kowal\Koszty\Koszty\Api\Data\KosztyInterface
     */
    public function setNazwaBazowa($nazwaBazowa);

    /**
     * Get koszt_stary
     * @return string|null
     */
    public function getKosztStary();

    /**
     * Set koszt_stary
     * @param string $kosztStary
     * @return \Kowal\Koszty\Koszty\Api\Data\KosztyInterface
     */
    public function setKosztStary($kosztStary);

    /**
     * Get koszt_nowy
     * @return string|null
     */
    public function getKosztNowy();

    /**
     * Set koszt_nowy
     * @param string $kosztNowy
     * @return \Kowal\Koszty\Koszty\Api\Data\KosztyInterface
     */
    public function setKosztNowy($kosztNowy);
}

