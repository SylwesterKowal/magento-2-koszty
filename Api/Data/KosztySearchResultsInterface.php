<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Api\Data;

interface KosztySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Koszty list.
     * @return \Kowal\Koszty\Api\Data\KosztyInterface[]
     */
    public function getItems();

    /**
     * Set store_id list.
     * @param \Kowal\Koszty\Api\Data\KosztyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

