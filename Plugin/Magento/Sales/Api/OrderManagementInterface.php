<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Plugin\Magento\Sales\Api;

use Kowal\Koszty\Service\Order;

class OrderManagementInterface
{
    public function __construct
    (
        \Kowal\Koszty\Service\Order $orderService
    )
    {
        $this->orderService = $orderService;
    }

    public function afterPlace(
        \Magento\Sales\Api\OrderManagementInterface $subject,
                                                    $result
    )
    {
        $this->orderService->setOrder($result)->execute();

        return $result;
    }
}

