# Mage2 Module Kowal Koszty

    ``kowal/module-koszty``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Koszty Produkcji

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_Koszty`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-koszty`
 - enable the module by running `php bin/magento module:enable Kowal_Koszty`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enabled (koszty/settings/enabled)

 - stores_id (koszty/settings/stores_id)


## Specifications

 - Console Command
	- Koszty

 - Controller
	- adminhtml > kowal_koszty/koszty/importuj

 - Helper
	- Kowal\Koszty\Helper\Query

 - Plugin
	- afterPlace - Magento\Sales\Api\OrderManagementInterface > Kowal\Koszty\Plugin\Magento\Sales\Api\OrderManagementInterface


## Attributes

 - Sales - koszt_stary (koszt_stary)

 - Sales - koszt_nowy (koszt_nowy)

 - Sales - nazwa_bazowa (nazwa_bazowa)

