<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

class Koszty extends Command
{

    const ORDER_ID = "order_id";
    const NAME_OPTION = "option";

    public function __construct
    (
        \Kowal\Koszty\Service\Order $orderService,
        OrderCollectionFactory      $collectionFactory
    )
    {
        $this->orderService = $orderService;
        $this->collectionFactory = $collectionFactory;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {

        try {
            $option = $input->getOption(self::NAME_OPTION);

            if ($order_id = $input->getArgument(self::ORDER_ID)) {

                $collection = $this->getOrdersCollection(['entity_id' => ['eq' => $order_id]]);
                foreach ($collection as $order) {
                    $result = $this->orderService->setOrder($order)->execute();
                    $output->writeln("TEST ORDER: " . $order->getEntityId()) . ' ' . $result;
                }

            } else {
                $orderIds = $this->orderService->query->getOrdersIds();
                $collection = $this->getOrdersCollection(['entity_id' => ['in' => [$orderIds]]]);
                foreach ($collection as $order) {
                    $result = $this->orderService->setOrder($order)->execute();
                    $output->writeln(date('Y-m-d H:i:s') . " ORDER: " . $order->getEntityId() . ' ' . $result);
                }
            }

        } catch (\Exception $e) {
            $output->writeln("ERROR " . $e->getMessage());
        }
        return 1;
    }

    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_koszty:koszty");
        $this->setDescription("Dodaj koszty produkcji do zamówień");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order ID"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

