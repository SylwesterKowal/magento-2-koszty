<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Query extends AbstractHelper
{

    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context);
    }





    public function getAttribute($sku, $code)
    {
        $product_id = $this->getProductIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        return $this->_getAttributeValue($product_id, $attributeId, $code);
    }



    public function getSKUbyAttribute($attribyte_code, $value)
    {
        try {
            $connection = $this->_getConnection('core_read');
            $sql = "SELECT e.sku 
                FROM " . $this->_getTableName('catalog_product_entity') . " e 
                LEFT JOIN " . $this->_getTableName('catalog_product_entity_varchar') . " v1 
                    ON e.entity_id = v1.entity_id AND v1.store_id = 0 
                    AND v1.attribute_id = (SELECT attribute_id FROM  " . $this->_getTableName('eav_attribute') . " 
                                            WHERE attribute_code = '$attribyte_code' 
                                            AND entity_type_id = (SELECT entity_type_id 
                                                                    FROM " . $this->_getTableName('eav_entity_type') . " 
                                                                    WHERE entity_type_code = 'catalog_product')) 
                    WHERE v1.value = ?";
            return $connection->fetchOne($sql, [$value]);
        } catch (Exception $e) {
            return $e->getTraceAsString() . "\r";
        }
    }

    public function getProductIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    public function checkIfSkuExists($sku, $producent = '')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT sku, type_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku IN (?,?)";
        return $connection->fetchRow($sql, [trim($sku), trim($producent) . '_' . trim($sku)]);
    }

    public function getOrdersIds()
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT order_id FROM " . $this->_getTableName('sales_order_item') . " WHERE nazwa_bazowa IS NULL AND created_at > ?";
        return $connection->fetchAssoc($sql, ['2023-12-31 00:00:00']);
    }

    public function getCostByNazwaBazowa($nazwaBazowa, $store_id = null)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT nazwa_bazowa, koszt_nowy, koszt_stary FROM " . $this->_getTableName('kowal_koszty_koszty') . " WHERE nazwa_bazowa = ? AND store_id = ?;";
        $koszty = $connection->fetchRow($sql, [$nazwaBazowa, $store_id]);
        if(isset($koszty['nazwa_bazowa']) && !empty($koszty['nazwa_bazowa'])) {
            return $koszty;
        }else{
            $sql = "SELECT nazwa_bazowa, koszt_nowy, koszt_stary FROM " . $this->_getTableName('kowal_koszty_koszty') . " WHERE nazwa_bazowa = ?;";
            return $connection->fetchRow($sql, [$nazwaBazowa]);
        }
    }

    public function updateCostInOrderItem($order_id, $sku, $nazwa_bazowa, $koszt_stary, $koszt_nowy)
    {
        $connection = $this->_getConnection('core_write');

            $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.nazwa_bazowa = ?,
                 t.koszt_stary = ?,
                 t.koszt_nowy = ?
                WHERE t.sku = ? AND t.order_id = ?  AND t.product_type != 'bundle'
                OR
                t.sku = ? AND t.order_id = ? AND t.parent_item_id  = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) ;";
            $connection->query($sql, [$nazwa_bazowa, $koszt_stary, $koszt_nowy, $sku, $order_id, $sku, $order_id]);

    }


    private function _getAttributeValue($productId, $attributeId, $attributeKey)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $productId));

        } catch (Exception $e) {
            return $e->getMessage() . ' ' . $attributeKey;
        }
    }
    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    private function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }
}

