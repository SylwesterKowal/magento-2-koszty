<?php

namespace Kowal\Koszty\Service;

use Magento\Framework\Exception\LocalizedException;

class Order
{
    private $order;

    public function __construct
    (
        \Kowal\Koszty\Helper\Query  $query,
        \Kowal\Koszty\Helper\Config $config,
        \Psr\Log\LoggerInterface    $logger
    )
    {
        $this->query = $query;
        $this->config = $config;
        $this->logger = $logger;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function execute()
    {
        try {
            if (!$this->config->isEnabled()) return "Moduł jest wyłączony.";
            if ($orderId = $this->order->getEntityId()) {
                $storeId = $this->order->getStoreId();
                $items = $this->order->getAllItems();
                foreach ($items as $item) {
                    if ($this->order->getStatus() == 'amazon_afn') {
                    } else {
                        if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                            continue;
                        }
                        if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                            continue;
                        }
                    }
                    $sku = $item->getSku();
                    $nazwaBazowa = $this->query->getAttribute($sku, 'nazwa_bazowa');
                    if ($koszty = $this->query->getCostByNazwaBazowa($nazwaBazowa, $storeId)) {
                        if (isset($koszty['nazwa_bazowa'])) {
                            $this->query->updateCostInOrderItem($orderId, $sku, $koszty['nazwa_bazowa'], $koszty['koszt_stary'], $koszty['koszt_nowy']);
                        }
                    }
                }
            }
        } catch (LocalizedException $e) {
            $this->logger->critical("KOSZTY:" . $e);
            return $e->getMessage();
        } catch (\Exception $e) {
            $this->logger->critical("KOSZTY" . $e);
            return $e->getMessage();
        }
    }
}