<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Model\ResourceModel\Koszty;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'koszty_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\Koszty\Model\Koszty::class,
            \Kowal\Koszty\Model\ResourceModel\Koszty::class
        );
    }
}

