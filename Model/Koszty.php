<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Model;

use Kowal\Koszty\Api\Data\KosztyInterface;
use Magento\Framework\Model\AbstractModel;

class Koszty extends AbstractModel implements KosztyInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\Koszty\Model\ResourceModel\Koszty::class);
    }

    /**
     * @inheritDoc
     */
    public function getKosztyId()
    {
        return $this->getData(self::KOSZTY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setKosztyId($kosztyId)
    {
        return $this->setData(self::KOSZTY_ID, $kosztyId);
    }

    /**
     * @inheritDoc
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getNazwaBazowa()
    {
        return $this->getData(self::NAZWA_BAZOWA);
    }

    /**
     * @inheritDoc
     */
    public function setNazwaBazowa($nazwaBazowa)
    {
        return $this->setData(self::NAZWA_BAZOWA, $nazwaBazowa);
    }

    /**
     * @inheritDoc
     */
    public function getKosztStary()
    {
        return $this->getData(self::KOSZT_STARY);
    }

    /**
     * @inheritDoc
     */
    public function setKosztStary($kosztStary)
    {
        return $this->setData(self::KOSZT_STARY, $kosztStary);
    }

    /**
     * @inheritDoc
     */
    public function getKosztNowy()
    {
        return $this->getData(self::KOSZT_NOWY);
    }

    /**
     * @inheritDoc
     */
    public function setKosztNowy($kosztNowy)
    {
        return $this->setData(self::KOSZT_NOWY, $kosztNowy);
    }
}

