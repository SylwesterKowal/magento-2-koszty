<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Koszty\Model;

use Kowal\Koszty\Api\Data\KosztyInterface;
use Kowal\Koszty\Api\Data\KosztyInterfaceFactory;
use Kowal\Koszty\Api\Data\KosztySearchResultsInterfaceFactory;
use Kowal\Koszty\Api\KosztyRepositoryInterface;
use Kowal\Koszty\Model\ResourceModel\Koszty as ResourceKoszty;
use Kowal\Koszty\Model\ResourceModel\Koszty\CollectionFactory as KosztyCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class KosztyRepository implements KosztyRepositoryInterface
{

    /**
     * @var KosztyCollectionFactory
     */
    protected $kosztyCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceKoszty
     */
    protected $resource;

    /**
     * @var Koszty
     */
    protected $searchResultsFactory;

    /**
     * @var KosztyInterfaceFactory
     */
    protected $kosztyFactory;


    /**
     * @param ResourceKoszty $resource
     * @param KosztyInterfaceFactory $kosztyFactory
     * @param KosztyCollectionFactory $kosztyCollectionFactory
     * @param KosztySearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceKoszty $resource,
        KosztyInterfaceFactory $kosztyFactory,
        KosztyCollectionFactory $kosztyCollectionFactory,
        KosztySearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->kosztyFactory = $kosztyFactory;
        $this->kosztyCollectionFactory = $kosztyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(KosztyInterface $koszty)
    {
        try {
            $this->resource->save($koszty);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the koszty: %1',
                $exception->getMessage()
            ));
        }
        return $koszty;
    }

    /**
     * @inheritDoc
     */
    public function get($kosztyId)
    {
        $koszty = $this->kosztyFactory->create();
        $this->resource->load($koszty, $kosztyId);
        if (!$koszty->getId()) {
            throw new NoSuchEntityException(__('Koszty with id "%1" does not exist.', $kosztyId));
        }
        return $koszty;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->kosztyCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(KosztyInterface $koszty)
    {
        try {
            $kosztyModel = $this->kosztyFactory->create();
            $this->resource->load($kosztyModel, $koszty->getKosztyId());
            $this->resource->delete($kosztyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Koszty: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($kosztyId)
    {
        return $this->delete($this->get($kosztyId));
    }
}

